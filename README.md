# Source du cours LaTeX 

Compiler le fichier ```latex.tex```

attention :

en cas de dépôt sur Overleaf, les fichiers temportaires (notamment les .bbl) sont éliminés lors du déploiement. Le fichier 

```
exemples/exArt.bbl 
```

a donc été copié dans 

```
exemples/exArt.bbb
```
